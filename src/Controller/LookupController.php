<?php

namespace Drupal\viewfield_argument_helper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class LookupController.
 */
class LookupController extends ControllerBase {

  /**
   * Lookup service.
   *
   * @var \Drupal\viewfield_argument_helper\Lookup
   */
  protected $viewfieldArgumentAutocompleteLookup;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->viewfieldArgumentAutocompleteLookup = $container->get('viewfield_argument_helper.lookup');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Lookup.
   */
  public function lookup(Request $request, $view_id, $display_id) {
    $slots = $this->viewfieldArgumentAutocompleteLookup->getArgumentOptions($view_id, $display_id);

    $element = [];
    $slot_count = 0;
    foreach ($slots as $slot => $opts) {
      $slot_count++;
      $element['slot_' . $slot] = [
        '#type' => 'details',
        '#title' => 'Slot ' . $slot_count . ': ' . $slot,
        '#attributes' => [
          'class' => [
            'vaa-slot-details',
          ],
        ],
      ];
      $element['slot_' . $slot]['options'] = [
        '#type' => 'table',
        '#attributes' => [
          'class' => [
            'vaa-lookup-table',
          ],
        ],
        '#header' => [
          'ID',
          'Label',
          'Bundle',
        ],
      ];
      foreach (array_values($opts) as $i => $opt) {
        $element['slot_' . $slot]['options'][$i]['id'] = [
          '#plain_text' => $opt['id'],
          '#title' => 'ID',
        ];
        $element['slot_' . $slot]['options'][$i]['label'] = [
          '#plain_text' => $opt['label'],
          '#title' => 'Label',
        ];
        $element['slot_' . $slot]['options'][$i]['bundle'] = [
          '#plain_text' => $opt['bundle'],
          '#title' => 'Bundle',
        ];
      }
    }

    return new JsonResponse([
      'markup' => $this->renderer->render($element),
      'slot_counts' => array_map('count', $slots),
    ]);
  }

}
