<?php

namespace Drupal\viewfield_argument_helper;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\views\Views;

/**
 * Utility functions to deal with argument entities.
 */
class Lookup {
  /**
   * Type manager.
   */
  protected $entityTypeManager;

  /**
   * Module handler service.
   */
  protected $moduleHandler;

  /**
   * Bundle info service.
   */
  protected $entityTypeBundleInfo;

  /**
   * Field manager service.
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * Return JSON list of argument options.
   *
   * This is an array of entities per argument.
   */
  public function getArgumentOptions($view_id = NULL, $display_id = NULL) {
    $results = [];
    $view = Views::getView($view_id);
    if (!$view) {
      return $results;
    }
    $view->setDisplay($display_id);
    $view->initHandlers();

    $slot_counter = 0;
    foreach ($view->argument as $key => $def) {
      $context = [
        'argument_plugin' => $def,
        'view_id' => $view_id,
        'display_id' => $display_id,
        'slot_count' => $slot_counter,
      ];
      $entity_type = $this->guessEntityType($def);
      $field_allowed_bundles = NULL;
      if (!$entity_type) {
        // Try to look up the entity type and allowed bundles from the field definition.
        if ($real_field = ($def->definition['field_name'] ?? NULL)) {
          $parent_entity_type = $def->getEntityType();
          $parent_entity_bundles = array_keys($this->entityTypeBundleInfo->getBundleInfo($parent_entity_type));
          $field_allowed_bundles = [];
          foreach ($parent_entity_bundles as $parent_bundle) {
            // Is the field on this bundle?
            $parent_fields = $this->entityFieldManager->getFieldDefinitions($parent_entity_type, $parent_bundle);
            if ($field_def = ($parent_fields[$real_field] ?? NULL)) {
              $entity_type = $field_def->getSettings()['target_type'] ?? NULL;
              // What target bundles are allowed for this definition?
              if ($bundle_field_target_bundles = ($field_def->getSettings()['handler_settings']['target_bundles'] ?? NULL)) {
                $field_allowed_bundles = array_merge($field_allowed_bundles, array_values($bundle_field_target_bundles));
              }
            }
          }
        }
      }
      $this->moduleHandler->alter('viewfield_argument_helper_entity_type', $entity_type, $context);
      // Still haven't figured out the target entity type? Bail out!
      if (!$entity_type) {
        $results[$key] = [];
        continue;
      }
      $bundles = $this->getAllowedBundles($def, $field_allowed_bundles);
      $this->moduleHandler->alter('viewfield_argument_helper_bundles', $bundles, $context);
      $entities = $this->getEntities($entity_type, $bundles);
      $this->moduleHandler->alter('viewfield_argument_helper_entities', $entities, $context);
      $results[$key] = $entities;
      $slot_counter++;
    }

    return $results;
  }

  /**
   * Guess the entity type from the argument plugin.
   */
  protected function guessEntityType($plugin) {
    // @TODO: add some more!
    switch ($plugin->getPluginId()) {
      case 'taxonomy_index_tid':
        return 'taxonomy_term';

      case 'node_nid':
        return 'node';

    }

    return NULL;
  }

  /**
   * Entity bundle constraints set by the argument validator.
   */
  protected function getAllowedBundles($plugin, $field_allowed_bundles = NULL) {
    $target_bundles = $plugin->options['validate_options']['bundles'] ?? NULL;
    if (!is_null($field_allowed_bundles)) {
      // No validation set on the argument plugin? Use the field limitations.
      if (is_null($target_bundles)) {
        $target_bundles = $field_allowed_bundles;
      }
      // Entities must be from the insection of field config and argument validation.
      else {
        $target_bundles = array_intersect($target_bundles, $field_allowed_bundles);
      }
    }
    return $target_bundles;
  }

  /**
   * Build a list of allowed options.
   */
  protected function getEntities($entity_type, $bundles = NULL) {
    $bundle_key = $this->entityTypeManager->getDefinition($entity_type)->getKey('bundle');
    $properties = $bundles ? [
      $bundle_key => $bundles,
    ] : [];
    $storage = $this->entityTypeManager->getStorage($entity_type);
    if (!$storage) {
      return NULL;
    }
    $entities = $storage->loadByProperties($properties);
    return array_map([$this, 'entityLabelMapper'], $entities);
  }

  /**
   * Returns an option value for the entity.
   */
  protected function entityLabelMapper($entity) {
    return [
      'id' => $entity->id(),
      'label' => $entity->label(),
      'bundle' => $entity->bundle(),
    ];
  }

}
