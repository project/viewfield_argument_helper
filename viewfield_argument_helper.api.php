<?php

/**
 * @file
 * Hook demos.
 */

/**
 * Alter entity type support by a particular argument plugin.
 *
 * @param string $entity_type Entity ID, like 'node'
 * @param array  $context Context array:
 *   [
 *     'argument_plugin' => Views argument object,
 *     'view_id'         => View ID,
 *     'display_id'      => View display ID,
 *     'slot_count'      => Argument numbering, 0..n,
 *   ]
 */
function hook_viewfield_argument_helper_entity_type_alter(string &$entity_type, array $context) {
  if ($context['view_id'] == 'events' && $context['display_id'] == 'block_1') {
    $entity_type = 'taxonomy_term';
  }
}

/**
 * Alter bundles allowed by a particular argument plugin.
 *
 * @param array  $bundles Entity bundles queried by lookup, defaulting to argument validator settings
 * @param array  $context Context array:
 *   [
 *     'argument_plugin' => Views argument object,
 *     'view_id'         => View ID,
 *     'display_id'      => View display ID,
 *     'slot_count'      => Argument numbering, 0..n
 *   ]
 */
function hook_viewfield_argument_helper_bundles_alter(array &$bundles, array $context) {
  if ($context['view_id'] == 'news_and_events') {
    unset($bundles['page']);
  }
}

/**
 * Alter entity options returned by the lookup.
 *
 * @param array $entities Entities returned by a lookup for an argument.
 * @param array $context Context array:
 *   [
 *     'argument_plugin' => Views argument object,
 *     'view_id'         => View ID,
 *     'display_id'      => View display ID,
 *     'slot_count'      => Argument numbering, 0..n,
 *   ]
 */
function hook_viewfield_argument_helper_entities_alter(array &$entities, array $context) {
  // For first argument for an events listing, ignore argument specs and load up a list of event_tags taxonomy terms.
  if ($context['view_id'] == 'events' && $context['slot_count'] == 0) {
    $entities = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => 'event_tags',
      ]);
  }
}
