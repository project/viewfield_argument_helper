(function ($) {
  function updateSlots($parent, $slot) {
    const view_id = $parent.find('select[name$="[target_id]"]').val();
    const display_id = $parent.find('select[name$="[display_id]"]').val();

    // Remove the old one.
    const $details = $slot.find('.vaa-slot-details');
    if ($details.length) {
      $details.remove();
    }

    if (view_id && display_id) {
      $parent.find('.vaa-loading').show();
      $.getJSON('/viewfield_argument_helper/lookup/' + view_id + '/' + display_id, function(data) {
        $parent.find('.vaa-loading').hide();
        if (data.slot_counts.length == 0) {
          const $message = $('<div class="vaa-slot-details"><p><em>No contextual arguments found on this display</em></p></div>');
          $slot.append($message);
        }
        else {
          $slot.append(data.markup);
          $slot.find('table').each(function () {
            $(this).addTableFilter({
              labelText: "Search available filters: ",
            });
          });
        }
      });
    }
  }

  Drupal.behaviors.vaa_update_slots = {
    attach: function (context, settings) {
      $(once('vaa_init', '.vaa-slots > div', context)).each(function () {
        const $slot = $(this);
        const $parent = $slot.parent().parent().closest('fieldset');

        const $loading = $('<img class="vaa-loading" src="' + settings.path.baseUrl + settings.viewfield_argument_helper.path + '/images/loading.gif">');
        $loading.hide();
        $slot.append($loading);

        updateSlots($parent, $slot);

        $parent.find('select').on('change', function(e) {
          updateSlots($parent, $slot);
        })
      });
    }
  };
})(jQuery);