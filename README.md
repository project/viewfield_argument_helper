Provides a helper/info widget for the Viewfield arguments textfield.

Making embedded Views can be tricky for content editors, particularly if they need to add contextual filtering. Viewfield exposes the contextual arguments as a text field, requiring the user to compose these arguments properly. For example "204,95/72" might mean pass (filter by) terms 204 OR 95 on the first contextual argument and node ID 72 on the second. But you have to know:

- there are two contextual filters configured for this View display
- the first is a term (which vocabularies?)
- the second is a node (which bundles?)
- how to figure out the term IDs and node IDs

In its first iteration, this module attempts to lookup available options (i.e., entities) for the contextual arguments, based on how they're configured in the View, and expose a list of options to the user. The user still has to compose the contextual argument string, but at least they don't have to hunt as hard for what tid to use. A contextual argument composition wizard UI is on the long term roadmap.